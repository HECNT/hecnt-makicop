var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var path = require('path');

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var loginRoute = require("./routes/login");

app.use('/login', loginRoute);

var path    = require("path");

app.get('/recuperar_contrasenna',function(req,res){
  res.sendFile(path.join(__dirname+'/../recuperar_contrasena.html'));
});

app.listen(3000, function(){
  console.log('Port : 3000');
});
